package com.example.student.performanceassessment;

import android.app.Fragment;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends SingleFragmentActivity {
    public ArrayList<RedditPost> RedditPosts = new ArrayList<RedditPost>();

    @LayoutRes
    @Override
    protected int getLayoutResId() {

        return R.layout.activity_fragment;
    }
    @Override
    protected RedditListFragment createFragment(){

        return new RedditListFragment();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
    }




}
